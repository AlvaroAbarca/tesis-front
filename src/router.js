import Vue from "vue";
import Router from "vue-router";

import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Projects from "./views/Projects.vue";

import AppHeader from "./components/NavBar.vue";
import AppFooter from "./components/Footer.vue";

import ERROR from "./views/ERROR.vue";

import store from "./store";
import { isNull } from "util";

Vue.use(Router);

let router = new Router({
  //base: process.env.BASE_URL,
  //linkExactActiveClass: "active",
  mode: "history",
  routes: [
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: { guest: true }
    },
    {
      path: "/home",
      name: "home",
      components: {
        header: AppHeader,
        default: Home,
        footer: AppFooter
      },
      meta: { requiresAuth: true }
    },
    {
      path: "/projects",
      name: "projects",
      components: {
        header: AppHeader,
        default: Projects,
        footer: AppFooter
      },
      meta: { requiresAuth: true }
    },
    {
      path: "*",
      components: {
        header: AppHeader,
        default: ERROR,
        footer: AppFooter
      },
      meta: { requiresAuth: true }
    }
  ]
});
router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("access_token");
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (token == "undefined" || token == "" || isNull(token)) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath }
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    console.log(token);
    if (token == "undefined" || token == "" || isNull(token)) {
      next("/login");
    } else {
      next();
    }
  } else {
    next();
  }
});
export default router;
