import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";
import es from "vuetify/es5/locale/es";

Vue.use(Vuetify, {
  iconfont: "md",
  lang: {
    locales: { es },
    current: "es"
  },
  theme: {
    primary: "#9652ff",
    success: "#3cd1c2",
    info: "#ffaa2c",
    error: "#f83e70",
    completes: "#3cd1c2",
    ongoings: "#ffaa2c",
    overdues: "#f83e70"
  }
});
