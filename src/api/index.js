import axios from "axios";

const api_client = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL
});
export { api_client };
