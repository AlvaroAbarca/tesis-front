import Vue from "vue";
import Vuex from "vuex";
import { api_client } from "./api";
import { async } from "q";
//import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    accessToken: localStorage.getItem("access_token") || "",
    refreshToken: localStorage.getItem("refresh_token") || "",
    currentUser: {} || JSON.parse(localStorage.getItem("current_user")),
    logged: false,
    IS_APP_ERROR: ""
  },
  getters: {
    GET_TOKEN: state => {
      return state.accessToken;
    },
    GET_RTOKEN: state => {
      return state.refreshToken;
    },
    GET_USER: state => {
      return state.currentUser;
    },
    IS_LOGGED: state => {
      return state.logged;
    }
  },
  mutations: {
    SET_TOKEN: (state, payload) => {
      //state.accessToken = token;
      //state.refreshToken = r_token;
      localStorage.setItem("access_token", payload.access);
      localStorage.setItem("refresh_token", payload.refresh);
    },
    update_token: (state, token) => {
      localStorage.setItem("accessToken", token);
    },
    SET_USER: (state, payload) => {
      localStorage.setItem("current_user", JSON.stringify(payload.user));
      state.currentUser = payload.user;
    }
  },
  actions: {
    login: async ({ commit, dispatch, state }, payload) => {
      return new Promise((resolve, reject) => {
        api_client
          .post("api/token/", payload, {
            validateStatus: function(status) {
              return status < 500;
            }
          })
          .then(resp => {
            if (resp.data.detail) {
              console.log("MENSAJE: ", resp.data.detail);
              reject(resp.data.detail);
            } else {
              console.log(resp.data);
              commit("SET_TOKEN", resp.data);
              resolve();
            }
          })
          .catch(err => {
            console.log(err);
            reject(err);
          });
      });
    },
    login_v2: async ({ commit, dispatch, state }, payload) => {
      return new Promise((resolve, reject) => {
        api_client
          .post("users/login/", payload, {
            validateStatus: function(status) {
              return status < 500;
            }
          })
          .then(resp => {
            if (resp.data.non_field_errors) {
              //console.log("MENSAJE: ", resp.data.detail);
              reject(resp.data.non_field_errors);
            } else {
              console.log(resp.data);
              commit("SET_TOKEN", resp.data);
              commit("SET_USER", resp.data);
              state.logged = true;
              resolve();
            }
          })
          .catch(err => {
            console.log(err);
            reject(err);
          });
      });
    },
    logout: async ({ commit, dispatch, state }, payload) => {
      return new Promise((resolve, reject) => {
        localStorage.removeItem("access_token");
        localStorage.removeItem("refresh_token");
        commit("SET_USER", {});
        localStorage.removeItem("current_user");
        resolve();
      });
    },
    refresh_token: ({ commit, dispatch, state }) => {
      api_client
        .post(
          "api/refresh/",
          { refresh: state.refreshToken },
          {
            validateStatus: function(status) {
              return status < 500;
            }
          }
        )
        .then(resp => {
          if (resp.data.detail) {
            console.log("MENSAJE: ", resp.data.detail);
          } else {
            console.log(resp.data);
            commit("update_token", resp.data.access);
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    //logout: {}
  }
});
